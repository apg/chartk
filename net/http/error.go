package http

import (
	"encoding/json"
	"net/http"
	"strings"

	"context"
)

// ErrorResponse responds with an error appropriately based on Accept header.
func ErrorResponse(ctx context.Context, w http.ResponseWriter, r *http.Request, err error) {
	var ct []contentType
	if header := r.Header.Get("Accept"); header != "" {
		ct = parseAccept(header)
	}

	if len(ct) > 0 {
		for _, c := range ct {
			switch {
			case strings.HasPrefix(c.Type, "application/json"):
				bs, err := json.Marshal(map[string]error{"error": err})
				if err != nil {
					w.Header().Set("Content-Type", "application/json")
					w.Write(bs)
					return
				}
			default:
			}
		}
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(err.Error()))
}
