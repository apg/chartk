package http

import (
	"sort"
	"strconv"
	"strings"
)

type contentType struct {
	Type string
	Q    float64
}

// Sort descending
type byQ []contentType

func (c byQ) Len() int {
	return len(c)
}
func (c byQ) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c byQ) Less(i, j int) bool {
	return c[i].Q > c[j].Q
}

// parseAccept parses just enough to be dangerous, but doesn't do extended params.
func parseAccept(h string) []contentType {
	var (
		err error
		ts  []contentType
	)
	h = strings.Replace(h, " ", "", -1)
	bits := strings.Split(h, ",")
	for _, t := range bits {
		params := strings.Split(t, ";")
		if len(params) > 1 && strings.Contains(t, ";q=") {
			value := params[0]
			q := 0.0
			for _, param := range params[1:] {
				if strings.HasPrefix(param, "q=") {
					q, err = strconv.ParseFloat(params[1][2:], 64)
					if err != nil {
						q = 0.0
					}
				} else {
					value = value + ";" + param
				}
			}
			ts = append(ts, contentType{Type: value, Q: q})
		} else if len(params) > 1 { // has another parameter, which we ignore...
			ts = append(ts, contentType{Type: params[0], Q: 1.0})
		} else if params[0] == "*/*" {
			ts = append(ts, contentType{Type: params[0], Q: 0.7})
		} else if strings.HasSuffix(params[0], "/*") {
			ts = append(ts, contentType{Type: params[0], Q: 0.8})
		} else {
			ts = append(ts, contentType{Type: params[0], Q: 0.9})
		}

		sort.Sort(byQ(ts))
	}

	return ts
}
