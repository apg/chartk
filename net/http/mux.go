// Portions of this file adapted from Go net/http/server.go licensed
// under a BSD-style license, with copyright below.

// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// ContextMux is an HTTP request multiplexer.
// It matches the URL of each incoming request against a list of registered
// patterns and calls the handler for the pattern that
// most closely matches the URL.
//
// Patterns name fixed, rooted paths, like "/favicon.ico",
// or rooted subtrees, like "/images/" (note the trailing slash).
// Longer patterns take precedence over shorter ones, so that
// if there are handlers registered for both "/images/"
// and "/images/thumbnails/", the latter handler will be
// called for paths beginning "/images/thumbnails/" and the
// former will receive requests for any other paths in the
// "/images/" subtree.
//
// Note that since a pattern ending in a slash names a rooted subtree,
// the pattern "/" matches all paths not matched by other registered
// patterns, not just the URL with Path == "/".
//
// If a subtree has been registered and a request is received naming the
// subtree root without its trailing slash, ContextMux redirects that
// request to the subtree root (adding the trailing slash). This behavior can
// be overridden with a separate registration for the path without
// the trailing slash. For example, registering "/images/" causes ContextMux
// to redirect a request for "/images" to "/images/", unless "/images" has
// been registered separately.
//
// Patterns may optionally begin with a host name, restricting matches to
// URLs on that host only.  Host-specific patterns take precedence over
// general patterns, so that a handler might register for the two patterns
// "/codesearch" and "codesearch.google.com/" without also taking over
// requests for "http://www.google.com/".

// Package http defines ContextMux which allows one to attach ContextHandlers
package http

import (
	"context"
	"net/http"
	"net/url"
	"path"
	"strings"
	"sync"
)

// MethodMask is a mask type for defining HTTP methods
type MethodMask uint16

const (
	// MethodGet is an integer equivalent of http.MethodGet
	MethodGet MethodMask = 1 << iota
	// MethodHead is an integer equivalent of http.MethodGet
	MethodHead
	// MethodPost is an integer equivalent of http.MethodGet
	MethodPost
	// MethodPut is an integer equivalent of http.MethodGet
	MethodPut
	// MethodPatch is an integer equivalent of http.MethodGet
	MethodPatch
	// MethodDelete is an integer equivalent of http.MethodGet
	MethodDelete
	// MethodConnect is an integer equivalent of http.MethodGet
	MethodConnect
	// MethodOptions is an integer equivalent of http.MethodGet
	MethodOptions
	// MethodTrace is an integer equivalent of http.MethodGet
	MethodTrace
)

// MethodMasks go from http.Method* to MethodMask values
var MethodMasks = map[string]MethodMask{
	http.MethodGet:     MethodGet,
	http.MethodHead:    MethodHead,
	http.MethodPost:    MethodPost,
	http.MethodPut:     MethodPut,
	http.MethodPatch:   MethodPatch,
	http.MethodDelete:  MethodDelete,
	http.MethodConnect: MethodConnect,
	http.MethodOptions: MethodOptions,
	http.MethodTrace:   MethodTrace,
}

// ContextMux also takes care of sanitizing the URL request path,
// redirecting any request containing . or .. elements or repeated slashes
// to an equivalent, cleaner URL.
type ContextMux struct {
	Ctx   context.Context
	mu    sync.RWMutex
	m     map[string]muxEntry
	hosts bool // whether any patterns contain hostnames
}

// Handler is like http.Handler, except it takes a context.Context
type Handler interface {
	ServeHTTP(ctx context.Context, w http.ResponseWriter, r *http.Request)
}

// HandlerFunc allows a simple function to be used as a Handler
type HandlerFunc func(ctx context.Context, w http.ResponseWriter, r *http.Request)

// ServeHTTP satisfies Handler for HandlerFunc
func (f HandlerFunc) ServeHTTP(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	f(ctx, w, r)
}

type muxEntry struct {
	explicit bool
	h        Handler
	pattern  string
	m        MethodMask
}

// NewContextMux allocates and returns a new ContextMux.
func NewContextMux(ctx context.Context) *ContextMux {
	return &ContextMux{
		Ctx: ctx,
		m:   make(map[string]muxEntry),
	}
}

// Does path match pattern?
func pathMatch(pattern, path string) bool {
	if len(pattern) == 0 {
		// should not happen
		return false
	}
	n := len(pattern)
	if pattern[n-1] != '/' {
		return pattern == path
	}
	return len(path) >= n && path[0:n] == pattern
}

// Return the canonical path for p, eliminating . and .. elements.
func cleanPath(p string) string {
	if p == "" {
		return "/"
	}
	if p[0] != '/' {
		p = "/" + p
	}
	np := path.Clean(p)
	// path.Clean removes trailing slash except for root;
	// put the trailing slash back if necessary.
	if p[len(p)-1] == '/' && np != "/" {
		np += "/"
	}
	return np
}

// Find a handler on a handler map given a path string
// Most-specific (longest) pattern wins
func (mux *ContextMux) match(path string) (h Handler, m MethodMask, pattern string) {
	var n = 0
	for k, v := range mux.m {
		if !pathMatch(k, path) {
			continue
		}
		if h == nil || len(k) > n {
			n = len(k)
			h = v.h
			m = v.m
			pattern = v.pattern
		}
	}
	return
}

// Handler returns the handler to use for the given request,
// consulting r.Method, r.Host, and r.URL.Path. It always returns
// a non-nil handler. If the path is not in its canonical form, the
// handler will be an internally-generated handler that redirects
// to the canonical path.
//
// Handler also returns the registered pattern that matches the
// request or, in the case of internally-generated redirects,
// the pattern that will match after following the redirect.
//
// If there is no registered handler that applies to the request,
// Handler returns a ``page not found'' handler and an empty pattern.
func (mux *ContextMux) Handler(r *http.Request) (h Handler, m MethodMask, pattern string) {
	mask, ok := MethodMasks[r.Method]
	if !ok {
		return MethodNotAllowedHandler(), MethodMask(0), ""
	}

	if r.Method != http.MethodConnect {
		if p := cleanPath(r.URL.Path); p != r.URL.Path {
			_, m, pattern = mux.handler(mask, r.Host, p)
			url := *r.URL
			url.Path = p
			return RedirectHandler(url.String(), http.StatusMovedPermanently), mask, pattern
		}
	}
	return mux.handler(mask, r.Host, r.URL.Path)
}

// handler is the main implementation of Handler.
// The path is known to be in canonical form, except for CONNECT methods.
func (mux *ContextMux) handler(method MethodMask, host, path string) (h Handler, m MethodMask, pattern string) {
	mux.mu.RLock()
	defer mux.mu.RUnlock()

	// Host-specific pattern takes precedence over generic ones
	if mux.hosts {
		h, m, pattern = mux.match(host + path)
	}
	if h == nil {
		h, m, pattern = mux.match(path)
	}
	if h == nil {
		h, m, pattern = NotFoundHandler(), MethodMask(0), ""
	}
	if m&method == 0 {
		h, m, pattern = MethodNotAllowedHandler(), MethodMask(0), ""
	}

	return
}

// ServeHTTP dispatches the request to the handler whose
// pattern most closely matches the request URL.
func (mux *ContextMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.RequestURI == "*" {
		if r.ProtoAtLeast(1, 1) {
			w.Header().Set("Connection", "close")
		}
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	h, _, _ := mux.Handler(r)
	h.ServeHTTP(mux.Ctx, w, r.WithContext(mux.Ctx))
}

// Handle registers the handler for the given pattern.
// If a handler already exists for pattern, Handle panics.
func (mux *ContextMux) Handle(mask MethodMask, pattern string, handler Handler) {
	mux.mu.Lock()
	defer mux.mu.Unlock()

	if pattern == "" {
		panic("http: invalid pattern " + pattern)
	}
	if handler == nil {
		panic("http: nil handler")
	}
	if mux.m[pattern].explicit {
		panic("http: multiple registrations for " + pattern)
	}

	mux.m[pattern] = muxEntry{explicit: true, h: handler, pattern: pattern, m: mask}

	if pattern[0] != '/' {
		mux.hosts = true
	}

	// Helpful behavior:
	// If pattern is /tree/, insert an implicit permanent redirect for /tree.
	// It can be overridden by an explicit registration.
	n := len(pattern)
	if n > 0 && pattern[n-1] == '/' && !mux.m[pattern[0:n-1]].explicit {
		// If pattern contains a host name, strip it and use remaining
		// path for redirect.
		path := pattern
		if pattern[0] != '/' {
			// In pattern, at least the last character is a '/', so
			// strings.Index can't be -1.
			path = pattern[strings.Index(pattern, "/"):]
		}
		url := &url.URL{Path: path}
		mux.m[pattern[0:n-1]] = muxEntry{
			h:       RedirectHandler(url.String(), http.StatusMovedPermanently),
			pattern: pattern,
			m:       mask,
		}
	}
}

// HandleFunc registers the handler function for the given pattern.
func (mux *ContextMux) HandleFunc(mask MethodMask, pattern string, handler func(context.Context, http.ResponseWriter, *http.Request)) {
	mux.Handle(mask, pattern, HandlerFunc(handler))
}

// Get installs handler for method GET
func (mux *ContextMux) Get(pattern string, handler interface{}) {
	mux.Handle(MethodGet, pattern, bless(handler))
}

// Head installs handler for method HEAD
func (mux *ContextMux) Head(pattern string, handler interface{}) {
	mux.Handle(MethodHead, pattern, bless(handler))
}

// Post installs handler for method POST
func (mux *ContextMux) Post(pattern string, handler interface{}) {
	mux.Handle(MethodPost, pattern, bless(handler))
}

// Put installs handler for method PUT
func (mux *ContextMux) Put(pattern string, handler interface{}) {
	mux.Handle(MethodPut, pattern, bless(handler))
}

// Patch installs handler for method PATCH
func (mux *ContextMux) Patch(pattern string, handler interface{}) {
	mux.Handle(MethodPatch, pattern, bless(handler))
}

// Delete installs handler for method DELETE
func (mux *ContextMux) Delete(pattern string, handler interface{}) {
	mux.Handle(MethodDelete, pattern, bless(handler))
}

// Connect installs handler for method CONNECT
func (mux *ContextMux) Connect(pattern string, handler interface{}) {
	mux.Handle(MethodConnect, pattern, bless(handler))
}

// Options installs handler for method OPTIONS
func (mux *ContextMux) Options(pattern string, handler interface{}) {
	mux.Handle(MethodOptions, pattern, bless(handler))
}

// Trace installs handler for method TRACE
func (mux *ContextMux) Trace(pattern string, handler interface{}) {
	mux.Handle(MethodTrace, pattern, bless(handler))
}

// NotFoundHandler returns a handler which responds with a 404
func NotFoundHandler() Handler {
	return HandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		http.Error(w, "404 page not found", http.StatusNotFound)
	})
}

// MethodNotAllowedHandler returns a handler which responds with a 405
func MethodNotAllowedHandler() Handler {
	return HandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
	})
}

// RedirectHandler returns a request handler that redirects
// each request it receives to the given url using the given
// status code.
//
// The provided code should be in the 3xx range and is usually
// StatusMovedPermanently, StatusFound or StatusSeeOther.
func RedirectHandler(url string, code int) Handler {
	return HandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, url, code)
	})
}

func bless(handler interface{}) Handler {
	switch handler.(type) {
	case func(context.Context, http.ResponseWriter, *http.Request):
		return HandlerFunc(handler.(func(context.Context, http.ResponseWriter, *http.Request)))
	case Handler:
		return handler.(Handler)
	case HandlerFunc:
		return handler.(HandlerFunc)
	case http.Handler:
		return HandlerFunc(func(_ context.Context, w http.ResponseWriter, r *http.Request) {
			handler.(http.Handler).ServeHTTP(w, r)
		})
	case http.HandlerFunc:
		return HandlerFunc(func(_ context.Context, w http.ResponseWriter, r *http.Request) {
			handler.(http.HandlerFunc)(w, r)
		})
	default:
		panic("not a handler")
	}
	return nil
}
