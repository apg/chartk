package http

import "testing"

func TestParseAccept(t *testing.T) {
	tests := []struct {
		Header string
		First  contentType
	}{
		{Header: "text/plain", First: contentType{Type: "text/plain", Q: 0.9}},
		{Header: "text/plain;q=0.8", First: contentType{Type: "text/plain", Q: 0.8}},
		{Header: "text/plain;q=0.8,application/json;q=0.9", First: contentType{Type: "application/json", Q: 0.9}},
		{Header: "text/plain;level=9,application/json;q=0.9", First: contentType{Type: "text/plain", Q: 1.0}},
		{Header: "*/*,application/json;q=0.2;level=3", First: contentType{Type: "*/*", Q: 0.7}},
		{Header: "text/*,text/plain;q=0.2", First: contentType{Type: "text/*", Q: 0.8}},
	}

	for _, test := range tests {
		cts := parseAccept(test.Header)
		if len(cts) == 0 {
			t.Error("Expected more than one parsed contentType, got 0")
		} else if cts[0] != test.First {
			t.Errorf("Expected %+v, got %+v", test.First, cts[0])
		}
	}
}
