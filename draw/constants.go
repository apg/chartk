package draw

import "image/color"

var (
	// HighlightColor should simply default to this
	HighlightColor  = color.RGBA{255, 0, 0, 255}
	HighlightRadius = 4

	AxisColor           = color.RGBA{0, 0, 0, 255}
	LineWidth           = 2.0
	YLabelPaddingWidth  = 80
	XLabelPaddingHeight = 40
	FontSize            = 12

	// White
	White = color.RGBA{255, 255, 255, 255}

	// AxisGray
	AxisGray = color.RGBA{100, 100, 100, 255}

	// Black
	Black = color.RGBA{0, 0, 0, 255}

	// Transparent
	Transparent = color.RGBA{255, 255, 255, 0}
)
