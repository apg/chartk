package font

import (
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
)

const (
	LiberationSans = "draw/font/LiberationSans-Regular.ttf"
)

func LoadFontFace(path string, size int) (font.Face, error) {
	fontBytes := MustAsset(path)

	f, err := truetype.Parse(fontBytes)
	if err != nil {
		return nil, err
	}
	face := truetype.NewFace(f, &truetype.Options{
		Size: float64(size),
	})

	return face, nil
}
