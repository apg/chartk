package raster

import (
	"fmt"
	"image"
	"image/color"
	"io"
	"math"
	"time"

	"golang.org/x/image/font"

	"github.com/fogleman/gg"
	cdraw "gitlab.com/apg/chartk/draw"
	cfont "gitlab.com/apg/chartk/draw/font"
	"gitlab.com/apg/chartk/model"
)

var fnt font.Face

type imgChart struct {
	ch *model.Chart
	dc *gg.Context
}

type pngChart struct {
	imgChart
}

func init() {
	var err error
	fnt, err = cfont.LoadFontFace(cfont.LiberationSans, cdraw.FontSize*2)
	if err != nil {
		panic("Error in LoadFontFace: " + err.Error())
	}
}

// NewPNG returns a renderer that will render `ch` with PNG encoding
func NewPNG(ch *model.Chart) cdraw.Renderer {
	dc := gg.NewContext(ch.Width*2, ch.Height*2)
	return &pngChart{
		imgChart: imgChart{
			ch: ch,
			dc: dc,
		},
	}
}

func (c *imgChart) Series(bounds image.Rectangle, d model.Data,
	stroke model.Stroke, fill, hl color.RGBA) {
	// No sense doing anything if no data.
	if len(d) <= 1 {
		return
	}

	width := bounds.Max.X - bounds.Min.X
	height := bounds.Max.Y - bounds.Min.Y

	tickX := float64(width) / float64(len(d)-1)
	tickY := float64(height) / float64(model.Ticks)

	minX := float64(bounds.Min.X)
	maxY := float64(bounds.Max.Y)

	// Keep track of the last point that had a value
	lastI := 0

	// First, fill it.
	c.dc.MoveTo(minX, maxY)
	for i, p := range d {
		if p == math.MaxFloat64 {
			break
		}
		lastI = i
		cx := minX + float64(i)*tickX
		cy := (float64(model.Ticks) - p) * tickY
		c.dc.LineTo(cx, cy)
	}
	c.dc.LineTo(minX+float64(lastI)*tickX, maxY)
	c.dc.SetColor(fill)
	c.dc.Fill()

	// Then, stroke it.
	c.dc.MoveTo(minX, (float64(model.Ticks)-d[0])*tickY)
	for i, p := range d {
		if p == math.MaxFloat64 {
			break
		}
		cx := minX + float64(i)*tickX
		cy := (float64(model.Ticks) - p) * tickY
		c.dc.LineTo(cx, cy)
	}
	c.dc.SetLineWidth(cdraw.LineWidth)
	setStroke(c.dc, stroke)
	c.dc.Stroke()

	// And, if the highlight is visible, draw it at centered at the last
	// point.
	if hl.A > 0 {
		c.dc.SetColor(hl)
		c.dc.DrawEllipse(
			minX+float64(lastI)*tickX,
			(float64(model.Ticks)-d[lastI])*tickY,
			float64(cdraw.HighlightRadius),
			float64(cdraw.HighlightRadius))
		c.dc.Fill()
	}
}

func (c *imgChart) Steps(bounds image.Rectangle, d model.Data,
	stroke model.Stroke, fill, hl color.RGBA) {
	// No sense doing anything if no data.
	if len(d) <= 1 {
		return
	}

	width := bounds.Max.X - bounds.Min.X
	height := bounds.Max.Y - bounds.Min.Y

	tickX := float64(width) / float64(len(d))
	tickY := float64(height) / float64(model.Ticks)

	minX := float64(bounds.Min.X)
	maxY := float64(bounds.Max.Y)

	// Keep track of the last point that had a value
	lastI := 0

	// First, fill it.
	c.dc.MoveTo(minX, maxY)
	for i, p := range d {
		if p == math.MaxFloat64 {
			break
		}
		lastI = i
		cx := float64(i) * tickX
		cy := (float64(model.Ticks) - p) * tickY
		c.dc.LineTo(minX+cx, cy)
		c.dc.LineTo(minX+cx+tickX, cy)
	}
	c.dc.LineTo(minX+float64(lastI+1)*tickX, maxY)
	c.dc.SetColor(fill)
	c.dc.Fill()

	// Then, stroke it.
	c.dc.MoveTo(minX, (float64(model.Ticks)-d[0])*tickY)
	for i, p := range d {
		if p == math.MaxFloat64 {
			break
		}
		cx := float64(i) * tickX
		cy := (float64(model.Ticks) - p) * tickY
		c.dc.LineTo(minX+cx, cy)
		c.dc.LineTo(minX+cx+tickX, cy)
	}
	c.dc.SetLineWidth(cdraw.LineWidth)
	setStroke(c.dc, stroke)
	c.dc.Stroke()

	// And, if the highlight is visible, draw it at centered at the last
	// point.
	if hl.A > 0 {
		c.dc.SetColor(hl)
		c.dc.DrawEllipse(
			minX+float64(lastI)*tickX,
			(float64(model.Ticks)-d[lastI])*tickY,
			float64(cdraw.HighlightRadius),
			float64(cdraw.HighlightRadius))
		c.dc.Fill()
	}
}

func (c *imgChart) Title(bounds image.Rectangle, txt string) {
	c.dc.SetColor(cdraw.AxisColor)

	if len(txt) > 0 {
		x := float64(bounds.Min.X + (bounds.Max.X-bounds.Min.X)/2)
		y := float64(bounds.Min.Y + (bounds.Max.Y-bounds.Min.Y)/2)
		c.dc.DrawStringAnchored(txt, x, y, 0.5, 0.5)
	}
}

func (c *imgChart) YAxis(bounds image.Rectangle, labels []string, right bool) {
	if right {
		c.dc.MoveTo(float64(bounds.Min.X), float64(bounds.Min.Y))
		c.dc.LineTo(float64(bounds.Min.X), float64(bounds.Max.Y))
	} else {
		c.dc.MoveTo(float64(bounds.Max.X), float64(bounds.Min.Y))
		c.dc.LineTo(float64(bounds.Max.X), float64(bounds.Max.Y))
	}
	c.dc.SetColor(cdraw.AxisColor)
	c.dc.SetLineWidth(cdraw.LineWidth)
	c.dc.Stroke()

	if len(labels) > 1 {
		x := float64(bounds.Max.X) - 4.0
		pos := 1.0
		if right {
			x = float64(bounds.Min.X) + 4.0
			pos = 0.0
		}

		yStep := float64(bounds.Max.Y-bounds.Min.Y) / float64(len(labels)-1)
		for i := 0; i < len(labels); i++ {
			ypos := 0.5
			if i == 0 {
				ypos = 1.0
			} else if i == len(labels)-1 {
				ypos = 0.0
			}
			c.dc.DrawStringAnchored(labels[i], x, float64(bounds.Min.Y)+float64(i)*yStep, pos, ypos)
		}
	}
}

func (c *imgChart) XAxis(bounds image.Rectangle, labels []string) {
	c.dc.MoveTo(float64(bounds.Min.X), float64(bounds.Min.Y))
	c.dc.LineTo(float64(bounds.Max.X), float64(bounds.Min.Y))
	c.dc.SetColor(cdraw.AxisColor)
	c.dc.SetLineWidth(cdraw.LineWidth)
	c.dc.Stroke()

	if len(labels) > 1 {
		y := float64(bounds.Min.Y) + (float64(bounds.Max.Y-bounds.Min.Y) / 2)

		xStep := float64(bounds.Max.X-bounds.Min.X) / float64(len(labels)-1)
		for i := 0; i < len(labels); i++ {
			pos := 0.5
			if i == 0 {
				pos = 0.0
			} else if i == len(labels)-1 {
				pos = 1.0
			}
			c.dc.DrawStringAnchored(labels[i], float64(bounds.Min.X)+float64(i)*xStep, y, pos, 0.5)
		}
	}
}

func (c *imgChart) Render(w io.Writer) error {
	var hasXLabels, hasYLabels bool

	titleY := 0
	topPadding := 0
	bottomPadding := 0
	leftPadding := 0
	rightPadding := 0

	width2 := c.ch.Width * 2
	height2 := c.ch.Height * 2

	// Work out padding so we can define our plot bounds.

	if c.ch.Title != "" && (c.ch.XMin != 0 || c.ch.XMax != 0) {
		hasXLabels = true
		titleY = 0
		topPadding = cdraw.XLabelPaddingHeight
		bottomPadding = cdraw.XLabelPaddingHeight
	} else if c.ch.Title != "" {
		titleY = height2 - cdraw.XLabelPaddingHeight
		bottomPadding = cdraw.XLabelPaddingHeight
	} else if c.ch.XMin != 0 || c.ch.XMax != 0 {
		hasXLabels = true
		bottomPadding = cdraw.XLabelPaddingHeight
	}

	if c.ch.YMin != 0 || c.ch.YMax != 0 {
		hasYLabels = true
		// We have Y labels. Where do they go?
		if c.ch.OnlyLeftY {
			rightPadding = 0
			leftPadding = cdraw.YLabelPaddingWidth
		} else if c.ch.OnlyRightY {
			rightPadding = cdraw.YLabelPaddingWidth
			leftPadding = 0
		} else {
			rightPadding = cdraw.YLabelPaddingWidth
			leftPadding = cdraw.YLabelPaddingWidth
		}
	}

	// Compute our chart area.
	plotBounds := image.Rectangle{
		image.Point{leftPadding, topPadding},
		image.Point{width2 - rightPadding, height2 - bottomPadding},
	}

	hl := cdraw.Transparent
	if c.ch.Highlight {
		hl = cdraw.HighlightColor
	}

	f := c.Series
	if c.ch.Step {
		f = c.Steps
	}

	// Draw data.
	f(plotBounds, c.ch.Data0, c.ch.Stroke0, c.ch.Fill0, hl)
	f(plotBounds, c.ch.Data1, c.ch.Stroke1, c.ch.Fill1, hl)
	f(plotBounds, c.ch.Data2, c.ch.Stroke2, c.ch.Fill2, hl)
	f(plotBounds, c.ch.Data3, c.ch.Stroke3, c.ch.Fill3, hl)
	f(plotBounds, c.ch.Data4, c.ch.Stroke4, c.ch.Fill4, hl)

	c.dc.SetFontFace(fnt)

	// Draw the title
	if c.ch.Title != "" {
		titleBounds := image.Rectangle{
			image.Point{leftPadding, titleY},
			image.Point{
				width2 - rightPadding,
				titleY + cdraw.XLabelPaddingHeight},
		}
		c.Title(titleBounds, c.ch.Title)
	}

	// If we have axis to draw, draw the bottom line
	if hasXLabels || hasYLabels {
		var (
			yLabels []string
			xLabels []string
		)
		if hasYLabels {
			yLabels = numericLabels(c.ch.YMin, c.ch.YMax, 5)
		}
		if hasXLabels {
			xLabels = timeLabels(c.ch.XMin, c.ch.XMax, c.ch.TZ, 5)
		}

		c.dc.SetDash(1.0)
		c.XAxis(image.Rectangle{
			image.Point{leftPadding, height2 - bottomPadding},
			image.Point{width2 - rightPadding, height2},
		}, xLabels)

		c.YAxis(image.Rectangle{
			image.Point{0, topPadding},
			image.Point{leftPadding, height2 - bottomPadding},
		}, yLabels, false)

		c.YAxis(image.Rectangle{
			image.Point{width2 - rightPadding, topPadding},
			image.Point{width2, height2 - bottomPadding},
		}, yLabels, true)
	}

	return nil
}

func (c *pngChart) Render(w io.Writer) error {
	c.imgChart.Render(w)
	return c.imgChart.dc.EncodePNG(w)
}

func numericLabels(min, max float64, count int) (out []string) {
	step := (max - min) / float64(count)

	for i := count - 1; i >= 0; i-- {
		val := (step * float64(i)) + min
		switch {
		case val > 1000000000000.0:
			out = append(out, fmt.Sprintf("%.2fT", val/1000000000000.0))
		case val > 1000000000.0:
			out = append(out, fmt.Sprintf("%.2fB", val/1000000000.0))
		case val > 1000000.0:
			out = append(out, fmt.Sprintf("%.2fM", val/1000000.0))
		case val > 10000.0:
			out = append(out, fmt.Sprintf("%.1fK", val/1000.0))
		case val > 1000.0:
			out = append(out, fmt.Sprintf("%.0f", val))
		case val > 100.0:
			out = append(out, fmt.Sprintf("%.1f", val))
		case val > 10.0:
			out = append(out, fmt.Sprintf("%.2f", val))
		default:
			out = append(out, fmt.Sprintf("%.4f", val))
		}
	}
	return
}

const (
	day   = time.Hour * 24
	month = day * 30
	year  = month * 12
)

func timeLabels(min, max float64, loc *time.Location, count int) (out []string) {
	tmin := time.Unix(int64(min), 0).In(loc)
	tmax := time.Unix(int64(max), 0).In(loc)

	step := tmax.Sub(tmin)

	fs := "H:mm:ss"
	switch {
	case step > year:
		fs = "2006"
	case step > month:
		fs = "Mon 2006"
	case step > day:
		fs = "Jan 2"
	case step > time.Hour:
		fs = "03:04:05PM"
	default:
		fs = "03:04:05PM"
	}

	for i := 0; i < count-1; i++ {
		val := tmin.Add(step * time.Duration(i))
		out = append(out, val.Format(fs))
	}
	out = append(out, tmax.Format(fs))
	return
}

func setStroke(dc *gg.Context, st model.Stroke) {
	dc.SetColor(st.RGBA)
	switch st.Type {
	case model.Solid:
		dc.SetDash(1.0)
	case model.Dotted:
		dc.SetDash(2.0, 6.0)
	case model.Dashed:
		dc.SetDash(10.0, 15.0)
	}
}
