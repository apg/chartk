package draw

import (
	"image"
	"image/color"
	"io"

	"gitlab.com/apg/chartk/model"
)

// Renderer renders a time series chart to an io.Writer
type Renderer interface {
	Series(bnds image.Rectangle, d model.Data, s model.Stroke, f, hl color.RGBA)
	Steps(bnds image.Rectangle, d model.Data, s model.Stroke, f, hl color.RGBA)
	Title(bnds image.Rectangle, txt string)
	YAxis(bnds image.Rectangle, labels []string, right bool)
	XAxis(bnds image.Rectangle, labels []string)
	Render(w io.Writer) error
}
