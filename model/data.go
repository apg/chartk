package model

import (
	"errors"
	"fmt"
	"math"
)

const (
	b62         = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	MaxDataSize = 1024
	Ticks       = 62
)

var (
	ErrBadData     = errors.New("improper encoding found")
	ErrMaxDataSize = fmt.Errorf("data size exceeded maximum of %d", MaxDataSize)
)

type Data []float64

func (d Data) Encode() string {
	min := math.MaxFloat64
	max := math.SmallestNonzeroFloat64

	for _, f := range d {
		if f > max {
			max = f
		} else if f < min {
			min = f
		}
	}

	bs := make([]byte, len(d))

	r := math.Dim(max, min)
	if r == 0 {
		for i := 0; i < len(d); i++ {
			bs[i] = b62[0]
		}
		return string(bs)
	}

	enclen := float64(len(b62) - 1)
	for i, y := range d {
		idx := int(enclen * (y - min) / r)
		if idx >= 0 && idx < len(b62) {
			bs[i] = b62[idx]
		} else {
			bs[i] = b62[0]
		}
	}

	return string(bs)
}

func (d *Data) Decode(s string) error {
	if len(s) > MaxDataSize {
		return ErrMaxDataSize
	}

	bs := []byte(s)
	*d = make(Data, len(bs))

	// Assumes that b62 is setup ALPHAalphadigits
	for i := 0; i < len(bs); i++ {
		var x byte
		if (bs[i] >= 'A') && (bs[i] <= 'Z') {
			x = bs[i] - 'A'
		} else if (bs[i] >= 'a') && (bs[i] <= 'z') {
			x = bs[i] - 'a' + 26
		} else if (bs[i] >= '0') && (bs[i] <= '9') {
			x = bs[i] - '0' + 52
		} else {
			return ErrBadData
		}
		(*d)[i] = float64(x)
	}
	return nil
}
