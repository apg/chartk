package model

import (
	"errors"
	"fmt"
	"image/color"
	"log"
	"math"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

var (
	ErrInternalError = errors.New("internal error")
)

type StrokeType int

const (
	Solid StrokeType = iota
	Dashed
	Dotted
)

type Stroke struct {
	color.RGBA
	Type StrokeType
}

type Chart struct {
	Width      int            `url:"w,required"`
	Height     int            `url:"h,required"`
	Data0      Data           `url:"d0,required"`
	Data1      Data           `url:"d1"`
	Data2      Data           `url:"d2"`
	Data3      Data           `url:"d3"`
	Data4      Data           `url:"d4"`
	Stroke0    Stroke         `url:"s0" default:"2C0540"`
	Stroke1    Stroke         `url:"s1" default:"FFB32F"`
	Stroke2    Stroke         `url:"s2" default:"F5812A"`
	Stroke3    Stroke         `url:"s3" default:"E8522E"`
	Stroke4    Stroke         `url:"s4" default:"B81A41"`
	Fill0      color.RGBA     `url:"f0"`
	Fill1      color.RGBA     `url:"f1"`
	Fill2      color.RGBA     `url:"f2"`
	Fill3      color.RGBA     `url:"f3"`
	Fill4      color.RGBA     `url:"f4"`
	XMin       float64        `url:"xmin"`
	XMax       float64        `url:"xmax"`
	YMin       float64        `url:"ymin"`
	YMax       float64        `url:"ymax"`
	TZ         *time.Location `url:"tz"`
	Title      string         `url:"t"`
	Step       bool           `url:"step"`
	Highlight  bool           `url:"hl"`
	OnlyLeftY  bool           `url:"ol"`
	OnlyRightY bool           `url:"or"`
}

func NewChart() *Chart {
	return &Chart{
		TZ: time.UTC,
		// TODO: Want configurable defaults.
		//       Not yet sure how to provide that.
	}
}

func (c *Chart) DecodeRequest(r *http.Request) error {
	// parse all the chart values

	val := reflect.ValueOf(c)
	if val.Kind() != reflect.Ptr {
		log.Println("not a reflect.Ptr")
		return ErrInternalError
	}

	st := val.Elem()
	if st.Kind() != reflect.Struct {
		log.Println("not a reflect.Struct")
		return ErrInternalError
	}

	typ := val.Elem().Type()

	for i := 0; i < st.NumField(); i++ {
		field := st.Field(i)
		typeField := typ.Field(i)

		if !field.CanSet() {
			continue
		}

		name, required := decodeURLTag(typeField.Tag.Get("url"))
		defval := typeField.Tag.Get("default")

		// Get the thing from the URL
		value := r.FormValue(name)
		if value == "" && defval != "" {
			value = defval
		}

		if required && value == "" {
			return fmt.Errorf("%s is required but was not set", name)
		}

		switch field.Kind() {
		case reflect.Int:
			if i, err := strconv.ParseInt(value, 10, 64); err != nil {
				if required {
					return fmt.Errorf("%s is required but not a valid integer", name)
				}
			} else if !field.OverflowInt(i) {
				field.SetInt(i)
			}
		case reflect.Float64:
			if f, err := strconv.ParseFloat(value, 64); err != nil {
				if required {
					return fmt.Errorf("%s is required but not a valid float", name)
				}
			} else {
				field.SetFloat(f)
			}
		case reflect.Bool:
			if value == "true" || value == "1" {
				field.SetBool(true)
			}
		case reflect.String:
			field.SetString(value)

		case reflect.Slice:
			data, ok := field.Interface().(Data)
			if ok {
				err := (&data).Decode(value)
				if err != nil {
					return err
				}

				field.Set(reflect.ValueOf(data))
			} else {
				return ErrInternalError
			}

		case reflect.Struct:
			t := field.Type()

			switch {
			case t.PkgPath() == "gitlab.com/apg/chartk/model" &&
				t.Name() == "Stroke":
				stroke := Stroke{Type: Solid}
				if len(value)%2 == 1 {
					stroke.RGBA = decodeRGBA(value[0 : len(value)-1])
					switch value[len(value)-1] {
					case '.':
						stroke.Type = Dotted
					case '-':
						stroke.Type = Dashed
					}
				} else {
					stroke.RGBA = decodeRGBA(value)
				}
				field.Set(reflect.ValueOf(stroke))

			case t.PkgPath() == "image/color" && t.Name() == "RGBA":
				c := decodeRGBA(value)
				field.Set(reflect.ValueOf(c))
			}

		case reflect.Ptr:
			t := field.Type().Elem()
			if t.Kind() == reflect.Struct {
				switch {
				case t.PkgPath() == "time" && t.Name() == "Location":
					location := decodeLocation(value)
					field.Set(reflect.ValueOf(location))
				default:
					log.Printf("Unknown type for element %T, pkg_path=%q, name=%q",
						t, t.PkgPath(), t.Name())
					return ErrInternalError
				}
			}
		default:
			log.Printf("Unknown kind. %v", field.Kind())
			return ErrInternalError
		}
	}

	c.stretchData()

	return nil
}

// The number of points in each series might be different. This
// will ensure it's the same across.
//
// TODO: rewrite this.
func (c *Chart) stretchData() {
	ls := []int{
		len(c.Data0),
		len(c.Data1),
		len(c.Data2),
		len(c.Data3),
		len(c.Data4),
	}

	max := 0
	for _, l := range ls {
		if l > max {
			max = l
		}
	}

	if len(c.Data0) > 0 && len(c.Data0) < max {
		for i := len(c.Data0); i < max; i++ {
			c.Data0 = append(c.Data0, math.MaxFloat64)
		}
	}

	if len(c.Data1) > 0 && len(c.Data1) < max {
		for i := len(c.Data1); i < max; i++ {
			c.Data1 = append(c.Data1, math.MaxFloat64)
		}
	}

	if len(c.Data2) > 0 && len(c.Data2) < max {
		for i := len(c.Data2); i < max; i++ {
			c.Data2 = append(c.Data2, math.MaxFloat64)
		}
	}

	if len(c.Data3) > 0 && len(c.Data3) < max {
		for i := len(c.Data3); i < max; i++ {
			c.Data3 = append(c.Data3, math.MaxFloat64)
		}
	}

	if len(c.Data4) > 0 && len(c.Data4) < max {
		for i := len(c.Data4); i < max; i++ {
			c.Data4 = append(c.Data4, math.MaxFloat64)
		}
	}
}

func decodeURLTag(tag string) (name string, required bool) {
	bits := strings.SplitN(tag, ",", 2)
	if len(bits) == 2 {
		name = bits[0]
		required = bits[1] == "required"
	} else {
		name = bits[0]
	}
	return
}

func decodeLocation(l string) *time.Location {
	location, err := time.LoadLocation(l)
	if err != nil {
		return time.UTC
	}
	return location
}

func decodeRGBA(s string) color.RGBA {
	var ok bool
	var r, g, b, a uint8
	if len(s) == 6 || len(s) == 8 {
		ok = parseHex(s[0:2], &r) &&
			parseHex(s[2:4], &g) &&
			parseHex(s[4:6], &b)
		a = 255 // if there's rgb, default to full a
	}
	if len(s) == 8 && ok {
		parseHex(s[6:8], &a)
	}

	return color.RGBA{R: r, G: g, B: b, A: a}
}

func parseHex(s string, val *uint8) bool {
	i, err := strconv.ParseUint(s, 16, 8)
	if err != nil {
		return false
	}
	*val = uint8(i)
	return true
}
