package api

import "errors"

// ErrUnsupportedContentType is an exported error instance
var ErrUnsupportedContentType = errors.New("unsupported Content-Type")
