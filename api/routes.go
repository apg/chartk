package api

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/apg/chartk/draw/raster"
	"gitlab.com/apg/chartk/model"
	mux "gitlab.com/apg/chartk/net/http"
)

// ChartHandler utilizes a chart to return a valid HTTP response.
type ChartHandler func(ctx context.Context, chart *model.Chart, w http.ResponseWriter)

// HandleChart decodes a chart and dispatches to a ChartHandler
func HandleChart(f ChartHandler) mux.HandlerFunc {
	return func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		chart := model.NewChart()
		err := chart.DecodeRequest(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}

		f(ctx, chart, w)
	}
}

// HandlePNG is a ChartHandler that can be passed to HandleChart
func HandlePNG(ctx context.Context, chart *model.Chart, w http.ResponseWriter) {
	buf := bytes.Buffer{}

	ren := raster.NewPNG(chart)
	err := ren.Render(&buf)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Server Error"))
		return
	}

	w.Header().Set("Context-Type", "image/png")
	written, err := io.Copy(w, &buf)
	if err != nil {
		log.Printf("pri=error at=ioutil.Copy err=%q written=%d", err, written)
	}
}

// HandleSVG is a ChartHandler that can be pass to HandleChart
func HandleSVG(ctx context.Context, chart *model.Chart, w http.ResponseWriter) {
	w.Header().Set("Context-Type", "image/svg+xml")
	w.WriteHeader(http.StatusNotFound)

	body := fmt.Sprintf(`<svg height="%d" width="%d" xmlns="http://www.w3.org/2000/svg"></svg>`,
		chart.Width*2, chart.Height*2)
	w.Write([]byte(body))
}
