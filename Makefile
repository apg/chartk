SRCTTF = $(wildcard draw/font/*.ttf)
BINFONTS = draw/font/data.go

all: bindata

bindata: $(BINFONTS)

draw/font/data.go: $(SRCTTF)
	go-bindata -o $@ -pkg font $^
