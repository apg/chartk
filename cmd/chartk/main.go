package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"context"

	"gitlab.com/apg/chartk/api"
	h "gitlab.com/apg/chartk/net/http"
)

func main() {
	ctx := context.Background()

	mux := h.NewContextMux(ctx)
	mux.Get("/a.png", api.HandleChart(api.HandlePNG))
	mux.Get("/a.svg", api.HandleChart(api.HandleSVG))

	server := &http.Server{
		Addr:           ":" + os.Getenv("PORT"),
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(server.ListenAndServe())
}
